<?php

require_once 'LeapYear.php';

echo "Leap year - solution validation" . PHP_EOL;

$validationData = [2016, 2015, 2000, 1604, 1603, 1600, 0, -4, -100, -400, -401, "ABCDE", "", null, 2020.0];

foreach ($validationData as $year) {
    try {
        echo "$year: " . (LeapYear::isYearLeap($year) ? "yes" : "no") . PHP_EOL;
    } catch (Exception $e) {
        echo 'Caught exception: ', $e->getMessage() . PHP_EOL;
    }
}