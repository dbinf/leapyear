<?php

/**
 * Static class for checking if year is leap or not
 *
 * @author Damian
 */
class LeapYear {

    /**
     * @param int $year - year which must be checked if it is leap or not
     * @return boolean value indicating if year is leap or not
     */
    public static function isYearLeap($year) {
        return (new self)->isYearLeap2($year);
    }

    private function isYearLeap2($year) {
        $this->isInputCorrect($year);

        $byFour = $this->isEvenlyDivisibleByFour($year);
        $byHunderd = $this->isNotEvenlyDivisibleByHundred($year);
        $byFourHunderd = $this->isEvenlyDivisibleByFourHundred($year);

        return (($byFour && $byHunderd) || $byFourHunderd);
    }

    private function isEvenlyDivisibleByFour($year) {
        return ($year % 4 == 0);
    }

    private function isNotEvenlyDivisibleByHundred($year) {
        return ($year % 100 != 0);
    }

    private function isEvenlyDivisibleByFourHundred($year) {
        return ($year % 400 == 0);
    }

    private function isInputCorrect($input) {
        if (!is_int($input)) {
            throw new Exception('Input must be an integer number.');
        }

        if ($input < 1600) {
            throw new Exception('Input cant be lower than 1600.');
        }
    }

}
