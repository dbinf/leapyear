<?php

/**
 * Unit tests for LeapYear class
 *
 * @author Damian
 */
require_once 'LeapYear.php';

class LeapYearTest extends PHPUnit_Framework_TestCase {

    public function testIsYearLeap() {
        $result = LeapYear::isYearLeap(2000);
        $expected = TRUE;

        $this->assertEquals($expected, $result);
    }

    public function testIsYearLeap2() {
        $result = LeapYear::isYearLeap(2015);
        $expected = FALSE;

        $this->assertEquals($expected, $result);
    }

}
